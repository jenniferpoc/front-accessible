image: makeorg/docker-arch-docker:latest

cache:
  paths:
  - node_modules/

stages:
  - check
  - build
  - deploy
  - merge

lint:
  stage: check
  image: node:10.9.0-alpine
  script:
    - yarn install
    - yarn lint

flow-static:
  stage: check
  image: node:10.9.0
  script:
    - yarn install
    - yarn flow

test:
  stage: check
  image: node:10.9.0-alpine
  script:
    - yarn install
    - yarn test-with-coverage-codacy

build:
  stage: build
  tags:
    - docker
  before_script:
    - export IMAGE_VERSION=`git rev-parse --short=10 HEAD`
    - export IMAGE=www-accessible
    - mkdir -p /var/lib/docker
    - mount -t tmpfs -o size=12G tmpfs /var/lib/docker
    - if [ ! -e /var/run/docker.sock ]; then DOCKER_DRIVER=overlay2 dockerd & fi
    - until docker ps; do echo "waiting for docker to be up..."; sleep 0.5; done
    - docker login "https://$NEXUS_URL" --username "$NEXUS_USER" --password "$NEXUS_PASSWORD"
    - echo $(docker pull $NEXUS_URL/$IMAGE:$IMAGE_VERSION)
  script:
    - >
      if [[ -z $(docker images -q $NEXUS_URL/$IMAGE:$IMAGE_VERSION) ]];then
        docker build --rm -t $NEXUS_URL/$IMAGE:$IMAGE_VERSION .
        docker push $NEXUS_URL/$IMAGE:$IMAGE_VERSION
      fi
    - docker tag $NEXUS_URL/$IMAGE:$IMAGE_VERSION $NEXUS_URL/$IMAGE:$CI_COMMIT_REF_NAME-latest
    - docker push $NEXUS_URL/$IMAGE:$CI_COMMIT_REF_NAME-latest

deploy-preproduction:
  stage: deploy
  environment:
    name: preproduction
  only:
    - preproduction
  script:
    - >
      curl -D - -X "POST"
      -H "Accept: application/json"
      -H "Content-Type: application/x-www-form-urlencoded"
      -H "X-Rundeck-Auth-Token: $RUNDECK_PREPROD_TOKEN"
      --data-urlencode "argString=-version $CI_COMMIT_REF_NAME-latest"
      $RUNDECK_PREPROD_URL/api/16/job/$RUNDECK_PREPROD_JOB_ID/run

deploy-production:
  stage: deploy
  environment:
    name: production
  only:
    - production
  script:
    - >
      curl -D - -X "POST"
      -H "Accept: application/json"
      -H "Content-Type: application/x-www-form-urlencoded"
      -H "X-Rundeck-Auth-Token: $RUNDECK_PROD_TOKEN"
      --data-urlencode "argString=-version $CI_COMMIT_REF_NAME-latest"
      $RUNDECK_PROD_URL/api/16/job/$RUNDECK_PROD_JOB_ID/run

merge-to-prod:
  stage: merge
  when: manual
  only:
    - preproduction
  allow_failure: false
  script:
    - mkdir -p ~/.ssh/
    - chmod 700 ~/.ssh
    - ssh-keyscan -t rsa gitlab.com > ~/.ssh/known_hosts
    - echo "${DEPLOY_PRIVATE_KEY}" > ~/.ssh/id_rsa
    - chmod 600 ~/.ssh/id_rsa
    - export CI_PUSH_REPO=`echo $CI_REPOSITORY_URL | perl -pe 's#.*@(.+?(\:\d+)?)/#git@\1:#'`
    - echo "Target repo is ${CI_PUSH_REPO}"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git remote set-url origin "${CI_PUSH_REPO}"
    - git checkout production
    - git merge --ff-only origin/preproduction
    - git push origin production:production

import { NOTIFICATION_CLOSE } from 'Constants/actionTypes';

export const clearNotification = () => ({ type: NOTIFICATION_CLOSE });

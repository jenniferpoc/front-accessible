// @flow

import React from 'react';
import i18next from 'i18next';

export const AccountActivationSuccessComponent = () => (
  <p>{i18next.t('activate_account.success')}</p>
);

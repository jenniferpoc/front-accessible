/* @flow */

import styled from 'styled-components';

export const UnstyledList = styled.ul`
  padding: 0;
  list-style: none;
`;

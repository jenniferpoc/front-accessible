/* @flow */

import React from 'react';
import MetaTags from 'Components/MetaTags';
import { PageWrapper } from 'Components/Elements/MainElements';

const HomePage = () => (
  <PageWrapper>
    <MetaTags />
    <h1>Homepage</h1>
  </PageWrapper>
);

export default HomePage;

/**
 * notifiaction content selector
 * @param {*} state
 */
export const selectNotificationContent = state => state.notification.contentType;

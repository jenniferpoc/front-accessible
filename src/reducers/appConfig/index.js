/* @flow */


export default function appConfig(state: Object = {}, action: Object) {
  switch (action.type) {
    default:
      return state;
  }
}

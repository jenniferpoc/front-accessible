// @flow

export type UserObject = {
  email: string,
  password: string,
  firstname: string,
  age: string,
  postalcode: string,
  profession: string
}

export type ErrorObject = {
  field: string,
  message: string
}

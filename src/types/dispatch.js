// @flow

export type DispatchString = {
  type: string
}

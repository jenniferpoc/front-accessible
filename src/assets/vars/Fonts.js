/* @flow */

export const MakeFonts = {
  RobotoCondensedBold: "'Roboto Condensed Bold', Arial, serif",
  RobotoBold: "'Roboto Bold', Arial, sans-serif",
  RobotoRegular: "'Roboto Regular', Arial, sans-serif"
};

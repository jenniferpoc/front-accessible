/* @flow */

export const BasicColors = {
  PureWhite: 'rgb(255, 255, 255)',
  PureBlack: 'rgb(0, 0, 0)'
};

export const MakeThemeColors = {
  Blue: 'rgb(37, 49, 134)',
  Red: 'rgb(232, 24, 68)'
};

export const IconColors = {
  Facebook: 'rgb(58, 89, 152)',
  Google: 'rgb(219, 68, 55)'
};

export const BackgroundColors = {
  LightBlack: 'rgb(51, 51, 51)',
  Grey: 'rgb(118,118,118)',
  LightGrey: 'rgb(242, 242, 242)',
  ExtraLightGrey: 'rgb(228, 228, 228)',
  NotFoundPage: 'rgb(230,230,240)'
};

export const BorderColors = {
  MediumGrey: 'rgba(0, 0, 0, 0.5)',
  LightGrey: 'rgba(0, 0, 0, 0.2)',
  ErrorRed: 'rgb(237, 24, 68)'
};

export const TextColors = {
  MediumGrey: 'rgba(0, 0, 0, 0.55)',
  LightGrey: 'rgb(242, 242, 242)'
};

export const ShadowColors = {
  BlackZeroFiveOpacity: 'rgba(0, 0, 0, 0.5)',
  BlackZeroThreOpacity: 'rgba(0, 0, 0, 0.3)',
  BlackZeroTwoOpacity: 'rgba(0, 0, 0, 0.2)'
};

export const VoteColors = {
  Agree: 'rgb(110, 182, 32)',
  Disagree: 'rgb(218, 0, 27)',
  Neutral: 'rgb(126,126,126)'
};

export const SocialNetworksColors = {
  Facebook: 'rgb(58, 89, 152)',
  Twitter: 'rgb(26, 145, 218)',
  LinkedIn: 'rgb(0, 119, 181)'
};

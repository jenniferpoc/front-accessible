export const Elements = {
  HeaderHeightMobile: 67,
  HeaderHeightDesktop: 75,
  FooterHeightMobile: 96,
  FooterHeightDesktop: 48,
  SequenceFooterHeightMobile: 91,
  SequenceFooterHeightDesktop: 78
};

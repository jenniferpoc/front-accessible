// @flow

/* Intro & overall layout */
export const DISPLAY_SEQUENCE: string = 'display-sequence';
export const CLICK_MAKEORG_LOGO: string = 'click-navbar-logo';
export const CLICK_CONSULTATION_LINK: string = 'click-consultation-link';
export const CLICK_PERSONNAL_DATA_LINK: string = 'click-personnal-data-link';
export const CLICK_CLOSE_PANNEL: string = 'click-close-pannel';

/* Sequence */
export const CLICK_START_SEQUENCE: string = 'click-start-sequence';
export const CLICK_PROPOSAL_VOTE: string = 'click-proposal-vote';
export const CLICK_PROPOSAL_UNVOTE: string = 'click-proposal-unvote';
export const CLICK_SEQUENCE_FIRST_VOTE: string = 'click-sequence-first-vote';
export const CLICK_PROPOSAL_QUALIFY: string = 'click-proposal-qualify';
export const CLICK_PROPOSAL_UNQUALIFY: string = 'click-proposal-unqualify';
export const CLICK_SEQUENCE_NEXT_CARD: string = 'click-sequence-next-card';
export const CLICK_SEQUENCE_PREVIOUS_CARD: string = 'click-sequence-previous-card';
export const SKIP_SIGNUP_CARD: string = 'skip-sign-up-card';
export const CLICK_PROPOSAL_PUSH_CARD_IGNORE:string = 'click-proposal-push-card-ignore';
export const DISPLAY_INTRO_CARD: string = 'display-intro-card';
export const DISPLAY_PROPOSAL_PUSH_CARD:string = 'display-proposal-push-card';
export const DISPLAY_SIGN_UP_CARD:string = 'display-sign-up-card';
export const DISPLAY_FINAL_CARD: string = 'display-final-card';

/* Sign up journey */
export const DISPLAY_AUTHENTIFICATION_FORM: string = 'display-authentification-form';
export const DISPLAY_SIGN_UP_FORM: string = 'display-signup-form';
export const DISPLAY_SIGN_IN_FORM: string = 'display-signin-form';
export const DISPLAY_FORGOTPASSWORD_FORM: string = 'display-forgotpassword-form';
export const AUTHEN_SOCIAL_SUCCESS: string = 'authen-social-success';
export const SIGN_UP_EMAIL_SUCCESS: string = 'signup-email-success';
export const SIGN_IN_EMAIL_SUCCESS: string = 'signin-email-success';
export const AUTHEN_SOCIAL_FAILURE: string = 'authen-social-failure';
export const SIGN_UP_EMAIL_FAILURE: string = 'signup-email-failure';
export const SIGN_IN_EMAIL_FAILURE: string = 'signin-email-failure';

/* Proposal submit journey */
export const CLICK_PROPOSAL_SUBMIT: string = 'click-proposal-submit';
export const DISPLAY_PROPOSAL_SUBMIT_VALIDATION: string = 'display-proposal-submit-validation';
export const CLICK_PROPOSAL_SUBMIT_FORM_OPEN: string = 'click-proposal-submit-form-open';
export const CLICK_SIGN_UP_CARD_EMAIL: string = 'click-sign-up-card-email';
export const CLICK_SIGN_UP_CARD_FACEBOOK: string = 'click-sign-up-card-facebook';
export const CLICK_SIGN_UP_CARD_GOOGLE: string = 'click-sign-up-card-google';
export const CLICK_SIGN_UP_CARD_SIGN_IN: string = 'click-sign-up-card-sign-in';

/* Moderation Text */
export const DISPLAY_MODERATION_TEXT: string = 'display-moderation-text';
export const CLICK_MODERATION_LINK: string = 'click-moderation-link';

/** Locations */
export const LOCATION_SEQUENCE = 'sequence';

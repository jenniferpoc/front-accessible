export const CGU_LINK = 'https://about.make.org/terms-of-use';
export const DATA_POLICY_LINK = 'https://about.make.org/data-use-policy';
export const MODERATION_CHARTER_LINK = 'https://about.make.org/moderation-charter';
export const LEGAL_NOTICE_LINK = 'https://about.make.org/legal-notice';
export const CONTACT_LINK = 'https://about.make.org/contact';

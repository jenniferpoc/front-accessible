{
  "meta": {
    "home": {
      "title": "Make.org, a public interest accelerator",
      "description": "Propose, vote, act: finding solutions together to the biggest challenges we’re facing today. Those that receive the most support will be put into action by Make.org and its partners.",
      "picture": "https://uploads-ssl.webflow.com/59833d390a24e50001b873d8/5b977480cd07ff475b914214_default%20meta%20card%20il%20faut.png"
    }
  },
  "common": {
    "connexion_label": "Log in",
    "register_label": "Sign up",
    "form": {
      "email_label": "E-mail",
      "password_label": "Password",
      "firstname_label": "First name",
      "age_label": "Age (optional)",
      "postalcode_label": "Postcode (optional)",
      "profession_label": "Profession (optional)",
      "email is not a valid email": "Invalid e-mail format",
      "Password must be at least 8 characters": "Your password must contain at least 8 characters.",
      "email_already_exist": "This e-mail address is already registered.",
      "api_error": "Oops! We’re having difficulty loading this page. Please try again later.",
      "required_field": "Please fill in this field."
    },
    "from": "on",
    "email": "E-mail",
    "propose": "Propose",
    "click_there": "click here",
    "open_new_window": "Open in a new window",
    "loading": "Please wait while it loads."
  },
  "header": {
    "logo_alt": "Go to the Make.org site"
  },
  "proposal_page": {
    "button_1": "participate",
    "button_2": "find out more",
    "share_text": "Support this proposal by sharing it:",
    "footer_text": "This proposal was submitted as part of the {{operation_name}} campaign"
  },
  "proposal_card": {
    "previous": "Previous proposal",
    "next": "Next proposal",
    "number": "Proposal number",
    "author": {
      "age": "{{age}} years"
    }
  },
  "intro_card": {
    "title": "Thousands of citizens are proposing solutions.",
    "description_1": "Give your opinion on these solutions & propose your own.",
    "description_2": "The best ones will determine the actions we take.",
    "button": "Start",
    "partnership": "in partnership with"
  },
  "final_card": {
    "title": "Thank you for your participation, <br/> you can now access the complete consultation.",
    "button": "See all of the proposals"
  },
  "sign_up_card": {
    "title": "Receive the results of the consultation and be informed about upcoming actions",
    "authentification-text": "Sign in with",
    "next-cta": "No thanks, I don't wish to be informed of the results"
  },
  "push_proposal_card": {
    "title": "Do you have a solution you'd like to propose on this subject?",
    "next-cta": "Not yet, I want to keep voting"
  },
  "proposal_submit": {
    "bait": "We should ",
    "title": "Submit your proposals by filling in this form. Keep your proposal under 140 characters:",
    "entred_chars": "characters entered",
    "available_chars": "characters available",
    "description": "Don't worry, we’ll fix any spelling mistakes.",
    "moderation_charter": "To find out more about our moderation guidelines,",
    "success": "Your proposal has been received. It will now be reviewed by our moderation team. You will receive an e-mail once it has been validated."
  },
  "vote": {
    "intro_title": "I want to vote on this proposal. ",
    "intro_text": "I:",
    "agree": "Agree",
    "disagree": "Disagree",
    "neutral": "Cast a blank vote",
    "label": "{{count}} vote",
    "label_plural": "{{count}} votes"
  },
  "unvote": {
    "title": "By clicking on the button below, I’m withdrawing my vote on this proposal:",
    "button": "I withdraw my vote"
  },
  "results": {
    "title": "The votes of others on this proposal are as follows:",
    "total_text": "The total number of votes is: ",
    "tooltipbutton": {
      "agree": "Display the results for: “I agree”",
      "disagree": "Display the results for: “I disagree”",
      "neutral": "Display the results for: “I cast a blank vote”"
    }
  },
  "qualification": {
    "title": "By clicking on the buttons below, I wish to be more specific on my vote:",
    "doable": "I love it",
    "likeIt": "Realistic",
    "platitudeAgree": "Trivial",
    "noWay": "Definitely not!",
    "impossible": "Infeasible",
    "platitudeDisagree": "Trivial",
    "doNotUnderstand": "Don't understand",
    "noOpinion": "No opinion",
    "doNotCare": "Indifferent"
  },
  "sequence": {
    "return": "Return to the proposals"
  },
  "authentification": {
    "title": "Sign up to validate your proposal",
    "description": "with",
    "commitment": "Make.org is committed to protecting your ",
    "personal_data": "personal data "
  },
  "login": {
    "title": "I already have an account",
    "social_connect": "Sign in with",
    "or": "or",
    "button_connect": "Sign in",
    "email_connect": "Sign in with my e-mail address",
    "forgot_password_title": "Oops, I’ve",
    "forgot_password_link": "forgotten my password.",
    "registration_title": "I don’t have an account,",
    "registration_link": "I want to create one.",
    "email_doesnot_exist": "We can't find an account associated with this e-mail address."
  },
  "register": {
    "title": "Create an account",
    "social_connect": "Sign up with",
    "or": "or",
    "subtitle": "Sign up using this form",
    "login_title": "I already have an account.",
    "login_link": "Log in",
    "cgu_text": "By signing up, you are agreeing to our {{cgu_link}} and consent to receive e-mails (not too many!) from Make.org.",
    "cgu": "terms of service"
  },
  "forgot_password": {
    "title": "Reset my password",
    "description": "Please provide us with the e-mail address associated with your Make.org account to receive a link to reset your password.",
    "success": "Thank you. An e-mail has been sent to you that will allow you to update your password.",
    "return": "Go back to ",
    "login_link": "the login screen",
    "send_link": "receive the e-mail"
  },
  "footer_sequence": {
    "with": "with",
    "see_more": "Find out more on the subject:",
    "link": "Find out more about this consultation"
  },
  "main-footer": {
    "menu": {
      "item-1": {
        "label": "Legal notices"
      },
      "item-2": {
        "label": "Terms of service"
      },
      "item-3": {
        "label": "Privacy policy"
      },
      "item-4": {
        "label": "Contact"
      }
    }
  },
  "pannel": {
    "close": "Close the drop-down panel"
  },
  "cookie_alert": {
    "text": "By continuing to browse this site, you agree to our {{cgu_link}}, our {{policy_link}}, as well as to the use of cookies on our site in order to improve our service.",
    "cgu": "terms of service",
    "policy": "privacy policy"
  },
  "activate_account": {
    "success": "Your account has been activated. You can now sign in",
    "bad_link": "This link is no longer valid."
  },
  "reset_password": {
    "title": "Create a new password",
    "info": "You may choose a new password.",
    "send_cta": "Confirm",
    "success": {
      "title": "Thanks! Your password has been updated.",
      "info": "You can now sign in."
    },
    "failure": {
      "bad_link": "This link is no longer valid.",
      "error_message": "Your password could not be changed."
    }
  },
  "not_found": {
    "intro": "oops",
    "title": "Error 404",
    "description": "We are sorry, the page you have requested no longer exists."
  }
}